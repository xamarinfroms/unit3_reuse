﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Unit3_ReUse
{
    public partial class View1 : ContentView
    {
        public event EventHandler MyClick;
        public string ButtnText
        {
            get { return btn.Text; }
            set { btn.Text = value; }
        }
        public View1()
        {
            InitializeComponent();
            this.btn.Clicked += (sender, e) =>
            {
                this.MyClick?.DynamicInvoke(sender, e);
            };
        }
    }
}
